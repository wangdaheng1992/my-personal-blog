const PROD_HOST = 'http://47.92.23.74:9000/api';
const DEV_HOST = '/api';

export const APP_ENV = process.env.REACT_APP_STAGE;
export const ENV = {
  DEV: 'development',
  PROD: 'production'
};

const appUrl = APP_ENV === ENV.PROD ? PROD_HOST : DEV_HOST;
export default appUrl;