import { isEmpty, get } from 'lodash';
const NORMAL_ERROR_MSG = '操作失败';

const createApiError = serverError => {
  const error = new Error();
  error.name = 'Api error';
  const { response } = serverError;
  error.response = response;
  if (isEmpty(response)) {
    error.message = NORMAL_ERROR_MSG;
  } else {
    const errorMessage = get(response, 'data.errorMessage');
    error.message = errorMessage === undefined ? NORMAL_ERROR_MSG : errorMessage;
  }
  return error;
};

export default createApiError;
