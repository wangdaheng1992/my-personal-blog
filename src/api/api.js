import axios from 'axios';
import baseUrl from './apiHost';
import history from './history';
import Text from '../constants/textConstants'

const api = axios.create({
  baseURL: baseUrl,
  timeout: 15000,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  }
});

api.interceptors.request.use(
  config => {
    if (!config.headers['dh-token']) {
      const token = localStorage.getItem('dh-token');
      console.log('token is ======', token)
      if (token) {
        config.headers['dh-token'] = token;
      }
      else{
        //如果不是登陆请求，并且不带token请求的话，直接跳转到主页面
        if(config['url'] !== Text.NOT_FILTER_URL){
          history.push('/');
        }
      }
    }
    return config;
  },
  error => Promise.reject(error)
);

// api.interceptors.response.use((response) => {
//   return response;
// }, function (error) {
//   if (error && error.response && error.response.data === 'USER_NOT_AUTHORIZATION') {
//     history.push('/');
//   }
//   return Promise.reject(error.response);
// });

export default api;