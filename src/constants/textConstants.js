const Text = {
  TOKEN_NAME: "dh-token",

  NOT_FILTER_URL: '/login',


  APP_TITLE: '武汉Office Tour语音导游系统',

  LIST_ALL_SCENIC: '全部景点',
  LIST_CREATE_SCENIC: '创建景点',
  LIST_HEADER_NAME: '景点名称',
  LIST_HEADER_COUNT: '访问次数',
  LIST_HEADER_BEACON: 'Beacon',
  LIST_HEADER_SWITCH: '关/开',

  PLEASE_SELECT: '请选择',
  TIP: '提示',
  BACK: '返回',
  OK: '确定',
  CANCEL: '取消',
  DELETE: '删除',
  EDIT: '编辑',
  LOGIN: '登 录',
  LOGIN_LOADING: '正在登录...',

  DETAIL_CREATE_SCENIC: '创建景点',
  DETAIL_EDIT_SCENIC: '编辑景点',
  DETAIL_VIEW_SCENIC: '景点详情',
  DETAIL_COL_SCENIC_NAME: '景点名称',
  DETAIL_COL_SCENIC_BEACON: 'Beacon',
  DETAIL_COL_SCENIC_UUID: 'UUID',
  DETAIL_COL_SCENIC_MAJOR: 'Major',
  DETAIL_COL_SCENIC_MINOR: 'Minor',
  DETAIL_COL_SCENIC_POSITION: '景点位置',
  DETAIL_COL_SCENIC_DESC: '景点详情',

  DELETE_SCENIC_TIP : '确定删除此景点吗？',

  TIP_NO_SCENIC_NAME: '请输入景点名称',
  TIP_NO_SCENIC_BEACON: '请选择Beacon',
  TIP_NO_SCENIC_UUID: '请输入Beacon UUID',
  TIP_NO_SCENIC_MAJOR: '请输入Beacon Major',
  TIP_NO_SCENIC_MINOR: '请输入Beacon Minor',
  TIP_NO_SCENIC_LOCATION: '请输入景点位置',
  TIP_NO_SCENIC_CONTENT: '请输入景点详情',

  LOGIN_TIP_USERNAME: '请输入登录名',
  LOGIN_TIP_PASSWORD: '请输入密码',

  STATE_ADD: 'add',
  STATE_EDIT:  'edit',
  STATE_VIEW: 'view',
};

export default Text;