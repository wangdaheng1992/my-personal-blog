import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route} from 'react-router-dom';
import Login from './component/login/Login';
import {routes} from './constants/routes';
import history from './api/history';
import BlogList from './component/Blog/BlogList';
import Home from './component/Home/Home';
import Admin from './component/admin/admin'

import './static/css/icomoon.css'
import './static/css/bootstrap.css'
import './static/css/animate.css'

ReactDOM.render(
  <Router history={history}>
    <div>
      <Route exact path={routes.LOGIN_PAGE} component={Login}/>
      <Route exact path={routes.TEST_PAGE} component={BlogList} />
      <Route exact path={routes.HOME_PAGE} component={Home} />
      <Route exact path={routes.ADMIN_PAGE} component={Admin} />
    </div>
  </Router>
  , document.getElementById('root'));