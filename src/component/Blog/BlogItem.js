import React, { Component } from 'react';
import './BlogItem.css'

class BlogItem extends Component{
    render(){
        return(
            <div className="col-md-3 col-sm-6 col-padding animate-box" data-animate-effect="fadeInLeft">
                <div className="blog-entry">
                    <a href="#" className="blog-img"><img src={this.props.value.imageUrl} className="img-responsive" alt="Free HTML5 Bootstrap Template by FreeHTML5.co" /></a>
                    <div className="desc">
                        <h3><a href="#">{this.props.value.title}</a></h3>
                        <span><small>by Admin </small> / <small> Web Design </small> / <small> <i className="icon-comment"></i> 14</small></span>
                        <p>{this.props.value.content}</p>
                        <a href="#" className="lead">Read More <i className="icon-arrow-right3"></i></a>
                    </div>
                </div>
            </div>
        )
    }
}

export default BlogItem;