import React,{Component} from 'react'
import '../../static/css/style.css'
import BlogItem from './BlogItem';
import Logout from './Logout';
import Header from '../common/header/Header';
import Footer from '../common/footer/Footer';
import api from '../../api/api';

class BlogList extends Component {

    constructor() {
        super();
        this.state = {
            blogs: []
        };
    }

    render() {
        return (
            <div id="fh5co-page">
                <a href="#" className="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                <aside id="fh5co-aside" role="complementary" className="border js-fullheight">

                    <h1 id="fh5co-logo"><a href="index.html">Marble</a></h1>
                    <Header></Header>
                    <Footer></Footer>

                    <Logout value={this.props}></Logout>
                </aside>

                <div id="fh5co-main">
                    <div className="fh5co-narrow-content">
                        <h2 className="fh5co-heading animate-box" data-animate-effect="fadeInLeft">Read Our Blog</h2>
                        <div className="row row-bottom-padded-md">
                            {
                                this.state.blogs.map((blog) => <BlogItem key = {blog.id} value= {blog}></BlogItem>)
                            }
                            
                        </div>
                    </div>

                    <div id="get-in-touch">
                        <div className="fh5co-narrow-content">
                            <div className="row">
                                <div className="col-md-4 animate-box" data-animate-effect="fadeInLeft">
                                    <h1 className="fh5co-heading-colored">Get in touch</h1>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
                                    <p className="fh5co-lead">Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                                    <p><a href="#" className="btn btn-primary">Learn More</a></p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }


    componentDidMount = () => {
        api.get('/blogs')
            .then(response => {
                this.setState({
                    blogs: response.data
                });

                console.log("state is +++++++", this.state);
            })
            .catch(function (error) {
                throw error
            });
    }
}

export default BlogList;