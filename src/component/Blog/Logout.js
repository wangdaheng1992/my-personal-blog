import './Logout.css'
import React, { Component } from 'react';
import AuthService from '../login/AuthService';

class Logout extends Component {

    constructor(){
        super()
        this.Auth = new AuthService();
    }

    render() {
        return (
            <div className='logout-wrapper'>
                <a className='logout-button' href="#" onClick={this.handleLogout.bind(this)}>Log out</a>
            </div>
        )
    }

    handleLogout =() =>{
        this.Auth.logout();
        this.props.value.history.replace("/");
    }
}


export default Logout;