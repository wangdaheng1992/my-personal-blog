
import React, { Component } from 'react';
class Footer extends Component {
    render() {
        return (
            <div className="fh5co-footer">
                <p>Copyright &copy; 2018.Thoughtworks All rights reserved.</p>
                <ul>
                    <li><a href="#"><i className="icon-facebook2"></i></a></li>
                    <li><a href="#"><i className="icon-twitter2"></i></a></li>
                    <li><a href="#"><i className="icon-instagram"></i></a></li>
                    <li><a href="#"><i className="icon-linkedin2"></i></a></li>
                </ul>
            </div>
        );
    }

}

export default Footer;