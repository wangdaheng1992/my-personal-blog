
import React, { Component } from 'react'
import { Link } from 'react-router-dom';
class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {
            active: 'home'
        }
    }

    onActive = path => {
        console.log('onclik', path)
        this.setState({ active: path })
    }

    render() {
        const { active } = this.state
        console.log('active', active)
        return (
            <nav id="fh5co-main-menu" role="navigation">
                <ul>
                    {['home', 'blog', 'portfolio', 'about', 'contact', 'admin'].map(
                        path => <li className={active === path ? 'fh5co-active' : ''} 
                            key={path} onClick={() => this.onActive(path)}><Link to={path}>{path}</Link></li>
                    )}
                    {/* <img src="https://dhwang-1256649202.cos.ap-guangzhou.myqcloud.com/bi06.jpg"></img> */}
                </ul>
            </nav>
        );
    }
}


export default Header;