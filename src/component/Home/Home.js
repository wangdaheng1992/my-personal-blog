

import React, { Component } from 'react';
import Footer from '../common/footer/Footer';
import Header from '../common/header/Header';
import Logout from '../Blog/Logout';
import Image from '../../static/images/img_bg_1.jpg'
import api from '../../api/api';

class Home extends Component {

    constructor() {
        super()
        this.state = {
            file: ""
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);

    }

    render() {
        return (
            <div id="fh5co-page">
                <a href="#" className="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                <aside id="fh5co-aside" role="complementary" className="border js-fullheight">

                    <h1 id="fh5co-logo"><a href="index.html">Marble</a></h1>
                    <Header></Header>

                    <Footer></Footer>
                    <Logout></Logout>
                </aside>

                <div id="fh5co-main">
                    <aside id="fh5co-hero" className="js-fullheight">
                        <div className="flexslider js-fullheight">
                            <ul className="slides">
                                <li style={{ backgroundImage: "url(" + Image + ")" }}>
                                    <div className="overlay"></div>
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-md-8 col-md-offset-2 text-center js-fullheight slider-text">
                                                <div className="slider-text-inner">
                                                    <h1>Intuitive <strong></strong> is How Give We the User New Superpowers</h1>
                                                    <p><a className="btn btn-primary btn-demo popup-vimeo" href=""> <i className="icon-monitor"></i> Live Preview</a> <a className="btn btn-primary btn-learn">Learn More<i className="icon-arrow-right3"></i></a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                {/* <li style="background-image: url(images/img_bg_2.jpg);">
                                    <div className="overlay"></div>
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-md-8 col-md-offset-2 text-center js-fullheight slider-text">
                                                <div className="slider-text-inner">
                                                    <h1>We are Happy to Create Newest Modern Websites</h1>
                                                    <p><a className="btn btn-primary btn-demo popup-vimeo" href="#"> <i className="icon-monitor"></i> Live Preview</a> <a className="btn btn-primary btn-learn">Learn More<i className="icon-arrow-right3"></i></a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li> */}
                                {/* <li style="background-image: url(images/img_bg_3.jpg);">
                                    <div className="overlay"></div>
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-md-8 col-md-offset-2 text-center js-fullheight slider-text">
                                                <div className="slider-text-inner">
                                                    <h1>Download our Free HTML5 Bootstrap Template</h1>
                                                    <p><a className="btn btn-primary btn-demo popup-vimeo" href=""> <i className="icon-monitor"></i> Live Preview</a> <a className="btn btn-primary btn-learn">Learn More<i className="icon-arrow-right3"></i></a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li> */}
                            </ul>
                        </div>
                    </aside>


                    <div id="get-in-touch">
                        <div className="fh5co-narrow-content">
                            <div className="row">
                                <div className="col-md-4 animate-box" data-animate-effect="fadeInLeft">
                                    <h1 className="fh5co-heading-colored">Get in touch</h1>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
                                    <p className="fh5co-lead">Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                                    <p><a href="#" className="btn btn-primary">Learn More</a></p>
                                </div>

                            </div>
                        </div>
                    </div>

                    <form onSubmit={this.handleFormSubmit} enctype="multipart/form-data">
                        <input type="file" id="file" name="myfile" ref={node => this.fileNode = node} />
                        <input className="login-form-submit" type="submit" value="上传" />
                        <input type="button" onClick={this.handCancel} value="取消" />
                    </form>

                    <img src = "https://dhwang-1256649202.cos.ap-guangzhou.myqcloud.com/bi06.jpg"></img>
                </div>
            </div>
        );
    }

    handleFormSubmit(e) {
        e.preventDefault()
        console.log(this.fileNode.files[0])
        const formData = new FormData();
        formData.append("file", this.fileNode.files[0])
        console.log("form is ", formData);
        api.post("/file", formData, {
            headers: {
                "Content-Type": "multipart/form-data"
            }
        });
    }

    handleChange(e) {
        var files = e.target.files
        var file = files[0]
        this.setState({file: file}, () => {
            console.log("file is ===", this.state);
        })
        
    }

    handCancel(e) {
        console.log("cancel");
    }
}

export default Home;