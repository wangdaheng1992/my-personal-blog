import Text from '../../constants/textConstants';
import api from '../../api/api';


export default class AuthService{
    constructor(domain) {
        this.domain = domain || '' // API server domain
        this.login = this.login.bind(this)
    }

    login(userName, password){
        return api.post('/login', {
            userName: userName,
            password: password
        })
        .then(response =>{
           return response;
        })
        .catch(function(error){
            alert(error);
        });
    }

    loggedIn() {
        // Checks if there is a saved token and it's still valid
        const token = this.getToken() // GEtting token from localstorage
        return !!token // handwaiving here
    }


    setToken(dhToken){
        localStorage.setItem(Text.TOKEN_NAME, dhToken)
    }

    getToken() {
        // Retrieves the user token from localStorage
        return localStorage.getItem(Text.TOKEN_NAME)
    }

    logout() {
        // Clear user token and profile data from localStorage
        localStorage.removeItem(Text.TOKEN_NAME);
    }
}