import React, { Component } from 'react';
import './Login.css';
import AuthService from './AuthService';

class Login extends Component {
    constructor(){
        super();
        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.Auth = new AuthService();
    }
    render() {
        return (
            <div className="login-center">
                <div className="login-card">
                    <h1>Login</h1>
                    <form onSubmit={this.handleFormSubmit}>
                        <input
                            className="login-form-item"
                            placeholder="Username goes here..."
                            name="userName"
                            type="text"
                            onChange={this.handleChange}
                        />
                        <input
                            className="login-form-item"
                            placeholder="Password goes here..."
                            name="password"
                            type="password"
                            onChange={this.handleChange}
                        />
                        <input
                            className="login-form-submit"
                            value="SUBMIT"
                            type="submit"
                        />
                    </form>
                </div>
            </div>
        );
    }

    handleChange(e){
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    }

    handleFormSubmit(e){
        console.log("call login");
        e.preventDefault();
      
        this.Auth.login(this.state.userName,this.state.password).then(res =>{
            if(res.status === 200){
                console.log("res++++++", res);
                this.Auth.setToken(res.headers['dh-token']);
                this.props.history.replace('/blog');
            }else{
                console.log("error");
                var error = new Error(res.statusText)
                throw error;
            }
        })
    }
    // We do not want to stay in the login page if we are already loggedIn. So add this componentWillMount method hook to prevent it.
    componentWillMount(){
        if(this.Auth.loggedIn())
            this.props.history.replace('/blog');
    }
}

export default Login;