import React, { Component } from 'react';
import './admin.css';
import '../login/Login.css'
import Logout from '../Blog/Logout';
import Header from '../common/header/Header';
import Footer from '../common/footer/Footer';
import api from '../../api/api';

class Admin extends Component {
    constructor() {
        super();
        this.state = {
            imageId: '',
            title: '',
            content: '',
            createTime: '',
            type: ''
        }
        this.handleBlogSubmit = this.handleBlogSubmit.bind(this)
        this.handleImageSubmit = this.handleImageSubmit.bind(this)
        this.handleForChange = this.handleForChange.bind(this)
        this.handleForChangeDate = this.handleForChangeDate.bind(this)
        this.handleForChangeSelect = this.handleForChangeSelect.bind(this)

    }

    handleImageSubmit(e) {
        console.log(this.fileNode.files[0])
        const formData = new FormData();
        formData.append("file", this.fileNode.files[0])
        console.log("form is ", formData);
        api.post("/file", formData, {
            headers: {
                "Content-Type": "multipart/form-data"
            }
        }).then(res => {
            console.log(res.data);
            this.state.imageId = res.data;
        })
    }
    handleBlogSubmit(e) {
        e.preventDefault()
        console.log("imageId is ====", this.state.imageId);
        console.log("state is ====", this.state)
        api.post("/blogs", {
            imageId: this.state.imageId,
            title: this.state.title,
            content: this.state.content,
            createTime: this.state.createTime,
            type: this.state.type
        }).then(() =>{
            console.log("create blog success!");
            this.props.history.push("/blog");
        })

    }

    handleForChange(e) {
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    }

    handleForChangeDate(e) {
        this.setState({
            createTime: e.target.value
        });
    }

    handleForChangeSelect(e) {
        this.setState({
            type: e.target.value
        });
    }

    render() {
        return (
            <div id="fh5co-page">
                <a href="#" className="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                <aside id="fh5co-aside" role="complementary" className="border js-fullheight">
                    <h1 id="fh5co-logo"><a href="index.html">Marble</a></h1>
                    <Header></Header>
                    <Footer></Footer>
                    <Logout value={this.props}></Logout>
                </aside>

                <div id="fh5co-main">
                    <div className="fh5co-narrow-content">
                        <div className="row">

                            <form className="form-horizontal" onSubmit={this.handleBlogSubmit}>
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1" className="col-sm-2 control-label">Blog title</label>
                                    <div className="col-sm-10">
                                        <input type="text" className="form-control" id="title" name="title" placeholder="title" onChange={this.handleForChange} />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleInputPassword1" className="col-sm-2 control-label">Blog content</label>
                                    <div className="col-sm-10">
                                        <textarea className="form-control" name="content" rows="3" placeholder="content" onChange={this.handleForChange}></textarea>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1" className="col-sm-2 control-label">Blog type</label>
                                    <div className="col-sm-10">
                                        <select className="form-control" onChange={this.handleForChangeSelect}>
                                            <option>请选择分类</option>
                                            <option value='1'>技术类</option>
                                            <option value='2'>食品类</option>
                                            <option value='3'>旅游类</option>
                                            <option value='4'>见闻类</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1" className="col-sm-2 control-label">Blog time</label>
                                    <div className='col-sm-10'>
                                        <input type='date' className="form-control" name="createTime" id='createTime' onChange={this.handleForChangeDate} />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="exampleInputFile" className="col-sm-2 control-label">File input</label>
                                    <div className="col-sm-10">
                                        <input type="file" id="file" ref={node => this.fileNode = node} />
                                        <p className="help-block">Here gere gere.</p>
                                        <input type="button" value="upload" onClick={this.handleImageSubmit} />
                                    </div>
                                </div>
                                <button type="submit" className="login-form-submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Admin;