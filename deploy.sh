#!/usr/bin/env bash

CONTAINER_NAME=personal-blog-web
DOCKER_HOST="docker -H tcp://134.175.133.115:2375"

if  [ "$($DOCKER_HOST images -aq $CONTAINER_NAME)" ]; then
    echo "delete exist image"
    $DOCKER_HOST rmi -f $CONTAINER_NAME
fi


$DOCKER_HOST build -t $CONTAINER_NAME .

if [ "$($DOCKER_HOST ps -aq -f name=$CONTAINER_NAME)" ]; then
    if [ "$($DOCKER_HOST ps -q -f name=$CONTAINER_NAME)" ]; then
        echo "current docker container is running, stop first"
        $DOCKER_HOST stop $CONTAINER_NAME
    fi
    echo "personal-blog existed,delete first"
    $DOCKER_HOST rm $CONTAINER_NAME
fi


$DOCKER_HOST run -d \
--restart always --name $CONTAINER_NAME -e VIRTUAL_HOST=www.dhwang.club \
-p 3000:3000 $CONTAINER_NAME


