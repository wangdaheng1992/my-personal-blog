FROM node:8.10.0

# Create app directory
RUN mkdir -p /src/app
WORKDIR /src/app

COPY package.json /src/app/package.json
RUN npm install --silent
RUN npm install react-scripts@1.1.1 -g --silent


# Bundle app source
COPY . /src/app
RUN yarn build

EXPOSE 3000

# defined in package.json
CMD [ "npm", "run", "run-proc" ]