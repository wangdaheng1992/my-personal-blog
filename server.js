const express = require('express');
var proxy = require('express-http-proxy');
const path = require('path');
const app = express();

app.use(express.static(path.join(__dirname, 'build')));

app.use('/', proxy('http://134.175.133.115:9999'));
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(3000, () => {
    console.log('server is running...')
});